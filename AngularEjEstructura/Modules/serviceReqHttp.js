angular
    .module('app')
    .service('serviceReqHttp',serviceFns);
        
        serviceFns.$inject = ['$http'];
        
        function serviceFns($http){
           //var vm = this;
            //GET
            this.getX = function(){
                return $http.get('http://exprf010:9091/echo');
            }
            //PUT

            this.putX = function(name){        
                return $http.put('http://exprf010:9091/bye', {name: name});
            }
            //POST

            this.postX = function(name){
                return $http.post('http://exprf010:9091/hi', {name:name});
            }

            //DELETE

            this.deleteX = function(id){
                return $http.delete('http://exprf010:9091/clean'+'?id='+id);
            }
            
        }        
        
