angular
    .module('app')
    .controller('deleteController', controllerFuncs);

    controllerFuncs.$inject = ['$scope', 'serviceReqHttp'];

    function controllerFuncs($scope, serviceReqHttp){
         
        //var vm = this;
        
        $scope.delElem = function(id){
            serviceReqHttp.deleteX(id).then(function(response){
                $scope.elementoDel = response.data;
            })
        }
        
        
    }