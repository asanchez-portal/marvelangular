var app = angular.module('app', ['ngRoute']); //ARRAY van las Dependencias

/*
angular
    .module('app'); //ARRAY van las Dependencias
    app.$inject = ['ngRoute'];
*/

app.config(function($routeProvider){
    $routeProvider
    .when("/get",{
        templateUrl: "Modules/Get/get.html",
        controller: "getController"
    })
    .when("/post",{
        templateUrl: "Modules/Post/post.html",
        controller: "postController"
    })
    .when("/put",{
        templateUrl: "Modules/Put/put.html",
        controller: "putController"
    })
    .when("/delete",{
        templateUrl: "Modules/Delete/delete.html",
        controller: "deleteController"
    })
    .otherwise({
        redirectTo:'/get'
    });
})


/*
app.controller('controllerMenu', ['$scope', 'service', function($scope, service){
    $scope.goPut = function(){
        
    }
    
}])


app.controller('controller',['$scope', 'service', function($scope, service){
    
    $scope.getElem = function(){
        service.getX().then(function(response){
            $scope.elementoCogido = response.data;
        })
    }
    
    $scope.putElem = function(elem){
        service.putX(elem).then(function(response){
            $scope.elementoActualizado = response.data;
        })
    }
    
    $scope.postElem = function(elem){
        service.postX(elem).then(function(response){
            $scope.elementoNuevo = response.data;
        })
    }
    
    $scope.delElem = function(id){
        service.deleteX(id).then(function(response){
            $scope.elementoDel = response.data;
        })
    }
    
    
    
}])

app.service('service', ['$http', function($http){
    
    //GET
    this.getX = function(){
        return $http.get('http://exprf010:9091/echo');
    }
    //PUT
    
    this.putX = function(name){        
        return $http.put('http://exprf010:9091/bye', {name: name});
    }
    //POST
    
    this.postX = function(name){
        return $http.post('http://exprf010:9091/hi', {name:name});
    }
    
    //DELETE
    
    this.deleteX = function(id){
        return $http.delete('http://exprf010:9091/clean'+'?id='+id);
    }                    
    
 
    
    
}])

*/